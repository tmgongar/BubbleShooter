﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text highScoreText;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text nBallsText;
    [SerializeField] private ChamberManager cm;

    private void Start()
    {
        if (highScoreText != null)
            highScoreText.text = "High Score: " + PlayerPrefs.GetInt("HighScore", 0);
        if (scoreText != null)
            scoreText.text = "Your Score: " + PlayerPrefs.GetInt("Score", 0);
    }

    private void Update()
    {
        if (scoreText != null && SceneManager.GetActiveScene().name == "SampleScene")
            scoreText.text = "Score: " + GameObject.FindObjectOfType<GameplayManager>().score;
        if (nBallsText != null && cm != null)
            nBallsText.text = (cm.nBalls + 1).ToString();
    }

    public void NewGame()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("SampleScene");
    }

    public void Continue()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
