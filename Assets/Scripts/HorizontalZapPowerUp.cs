﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HorizontalZapPowerUp : MonoBehaviour
{
    [SerializeField] private float shootForce;
    [SerializeField] private Tilemap map;
    [SerializeField] private GameplayManager gm;
    [SerializeField] private PowerUpManager puM;

    private Rigidbody2D rb2d;
    private bool canShoot;
    private Vector3 hitPosition;

    private void Awake()
    {
        canShoot = true;
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) && canShoot == true)
            Shoot();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 8) return;

        rb2d.velocity = Vector2.zero;
        map = collision.gameObject.GetComponent<Tilemap>();
        hitPosition = Vector3.zero;
        foreach (ContactPoint2D hit in collision.contacts)
        {
            hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
            hitPosition.y = hit.point.y - 0.01f * hit.normal.y;
        }
        Vector3Int cellPos = map.WorldToCell(hitPosition);

        for (int i = 0; i < 9; i++)
        {
            Vector3Int bubbleRow1 = new Vector3Int(i - 4, cellPos.y, 0);
            if (map.GetTile<Tile>(bubbleRow1) != null)
            {
                if (map.GetTile<Tile>(bubbleRow1).color == Color.red) gm.score += 5;
                else if (map.GetTile<Tile>(bubbleRow1).color == Color.green) gm.score += 7;
                else if (map.GetTile<Tile>(bubbleRow1).color == Color.blue) gm.score += 10;

                map.SetTile(bubbleRow1, null);
            }


            Vector3Int bubbleRow2 = new Vector3Int(i - 4, cellPos.y + 1, 0);
            if(map.GetTile<Tile>(bubbleRow2) != null)
            {
                if (map.GetTile<Tile>(bubbleRow2).color == Color.red) gm.score += 5;
                else if (map.GetTile<Tile>(bubbleRow2).color == Color.green) gm.score += 7;
                else if (map.GetTile<Tile>(bubbleRow2).color == Color.blue) gm.score += 10;

                map.SetTile(bubbleRow2, null);
            }
        }

        puM.ballToPlay.SetActive(true);
        this.gameObject.SetActive(false);
    }

    private void Shoot()
    {
        canShoot = false;

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mouseDir = mousePos - transform.position;
        mouseDir.z = 0.0f;
        mouseDir = mouseDir.normalized;

        rb2d.AddForce(mouseDir * shootForce, ForceMode2D.Impulse);
    }
}
