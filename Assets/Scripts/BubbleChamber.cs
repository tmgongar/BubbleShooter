﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BubbleChamber : MonoBehaviour
{
    [SerializeField] private ChamberManager cm;
    [SerializeField] private GameplayManager gm;

    [SerializeField] private int maxBalls = 20;
    [SerializeField] private GameObject[] balls;
    [SerializeField] private Tilemap map;
    [SerializeField] private int minXTile, minYTile, maxXTile, maxYTile;

    private Color[,] gridOfPlay;
    private Color color1, color2, color3;
    private int nColor;

    private void Awake()
    {
        gridOfPlay = new Color[maxXTile - minXTile, maxYTile - minYTile];
    }

    private void Start()
    {
        cm.nBalls = maxBalls;
        SetBallsToPlay(maxBalls);
    }

    public void SetBallsToPlay(int nBalls)
    {
        if (nBalls == maxBalls)
        {
            GameObject ballActive1 = balls[nBalls - 1];
            GameObject ballActive2 = balls[nBalls - 2];

            cm.nBalls--;

            ballActive1.SetActive(true);
            ballActive1.GetComponent<BubbleController>().enabled = true;

            ballActive2.SetActive(true);

            ballActive1.transform.position = Vector3.zero;
            ballActive2.transform.position = Vector3.right + Vector3.down;

            SetColor(ballActive1.GetComponent<SpriteRenderer>());
            SetColor(ballActive2.GetComponent<SpriteRenderer>());

            if (ballActive1.GetComponent<SpriteRenderer>().color == new Color(0, 0, 0, 0))
            {
                gm.Win();
            }
        }
        else if (nBalls > 1 && nBalls < maxBalls)
        {
            GameObject ballActive1 = balls[nBalls - 1];
            GameObject ballActive2 = balls[nBalls - 2];
            GameObject ballDesactive = balls[nBalls];

            cm.nBalls--;

            ballDesactive.SetActive(false);
            ballDesactive.GetComponent<BubbleController>().enabled = false;
            ballActive1.GetComponent<BubbleController>().enabled = true;

            ballActive2.SetActive(true);

            ballActive1.transform.position = Vector3.zero;
            ballActive2.transform.position = Vector3.right + Vector3.down;

            SetColor(ballActive1.GetComponent<SpriteRenderer>());
            SetColor(ballActive2.GetComponent<SpriteRenderer>());

            if (ballActive1.GetComponent<SpriteRenderer>().color == new Color(0, 0, 0, 0))
            {
                gm.Win();
            }
        }
        else if (nBalls == 1)
        {
            GameObject ballActive1 = balls[nBalls - 1];
            GameObject ballDesactive = balls[nBalls];

            cm.nBalls--;

            ballDesactive.SetActive(false);
            ballActive1.SetActive(true);
            ballActive1.GetComponent<BubbleController>().enabled = true;

            ballActive1.transform.position = Vector3.zero;

            SetColor(ballActive1.GetComponent<SpriteRenderer>());

            if (ballActive1.GetComponent<SpriteRenderer>().color == new Color(0, 0, 0, 0))
            {
                gm.Win();
            }
        }
    }

    private void SetColor(SpriteRenderer sr)
    {
        color1 = new Color(0, 0, 0, 0);
        color2 = new Color(0, 0, 0, 0);
        color3 = new Color(0, 0, 0, 0);

        for (int i = 0; i < maxXTile - minXTile; i++)
        {
            for (int j = 0; j < maxYTile - minYTile; j++)
            {
                if (map.GetTile<Tile>(new Vector3Int(i + minXTile, j + minYTile, 0)) != null)
                    gridOfPlay[i, j] = map.GetTile<Tile>(new Vector3Int(i + minXTile, j + minYTile, 0)).color;

                if (gridOfPlay[i, j] != new Color(0, 0, 0, 0) && color1 == new Color(0, 0, 0, 0))
                {
                    color1 = gridOfPlay[i, j];
                    nColor = 1;
                }
                else if (gridOfPlay[i, j] != new Color(0, 0, 0, 0) && color1 != new Color(0, 0, 0, 0) && color1 != gridOfPlay[i, j] && color2 == new Color(0, 0, 0, 0))
                {
                    color2 = gridOfPlay[i, j];
                    nColor = 2;
                }
                else if (gridOfPlay[i, j] != new Color(0, 0, 0, 0) && color1 != new Color(0, 0, 0, 0) && color1 != gridOfPlay[i, j] && color2 != new Color(0, 0, 0, 0) && color2 != gridOfPlay[i, j] && color3 == new Color(0, 0, 0, 0))
                {
                    color3 = gridOfPlay[i, j];
                    nColor = 3;
                }
            }
        }

        // Reset the grid
        for (int i = 0; i < maxXTile - minXTile; i++)
        {
            for (int j = 0; j < maxYTile - minYTile; j++)
            {
                gridOfPlay[i, j] = new Color(0, 0, 0, 0);
            }
        }

        int randomColor = Random.Range(1, nColor + 1);
        if (randomColor == 1) sr.color = color1;
        if (randomColor == 2) sr.color = color2;
        if (randomColor == 3) sr.color = color3;

        if (nColor == 0)
            sr.color = new Color(0, 0, 0, 0);
    }
}
