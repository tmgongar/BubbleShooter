﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BubbleController : MonoBehaviour
{
    [Header("--Private References--")]
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    private BubbleChamber bubbleChamber;

    [Header("--Public References--")]
    [SerializeField] private ChamberManager cm;
    [SerializeField] private GameplayManager gm;
    [SerializeField] private Tilemap map;
    [SerializeField] private Tile greenBubble, redBubble, blueBubble;
    

    [Header("--Shoot Force--")]
    [SerializeField] private float shootForce;

    [Header("--Private Variables--")]
    private Tile colorBubble;
    private Vector3 hitPosition;
    private Vector3 startPosition;
    private Vector3Int ballInTilemapPosition;
    private bool canShoot;
    private int nBalls;
    private int points = 0;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        bubbleChamber = GameObject.FindObjectOfType<BubbleChamber>();
        startPosition = transform.position;
        canShoot = true;
    }

    private void Update()
    {
        // Shoot if mousebuttonup
        if (Input.GetMouseButtonUp(0) && canShoot == true)
            Shoot();
    }

    // Check collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Collision limit then return
        if (collision.gameObject.layer == 8) return;
        if (collision.gameObject.layer == 9) return;

        /// Collision bubbles, make maths
        /// 1. Stop the ball
        /// 2. Define Tilemap
        /// 3. Initialize hitPosition
        /// 4. Global coordinates
        /// 5. Make those to cell coordinates in Tilemap
        rb2d.velocity = Vector2.zero;
        map = collision.gameObject.GetComponent<Tilemap>();
        hitPosition = Vector3.zero;
        foreach (ContactPoint2D hit in collision.contacts)
        {
            hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
            hitPosition.y = hit.point.y - 0.01f * hit.normal.y;
        }
        Vector3Int cellPos = map.WorldToCell(hitPosition);
        
        // Check y cellPos.y is odd or even        
        bool isPar = (cellPos.y % 2) == 0 ? true : false;

        /// Tile color
        if (sr.color == greenBubble.color) colorBubble = greenBubble;
        else if (sr.color == redBubble.color) colorBubble = redBubble;
        else if (sr.color == blueBubble.color) colorBubble = blueBubble;

        // Directions after ball hits
        Vector3Int
            left = Vector3Int.left,
            right = Vector3Int.right,
            down = Vector3Int.down,
            downLeft = Vector3Int.down + Vector3Int.left,
            downRight = Vector3Int.down + Vector3Int.right,
            up = Vector3Int.up,
            upLeft = Vector3Int.up + Vector3Int.left,
            upRight = Vector3Int.up + Vector3Int.right;

        /// Even: cellPos less than ballPos then add down, if greater add downLeft
        /// Odd: cellPos less than ballPos then add downRight, if greater add down
        if (isPar)
        {
            if(map.CellToWorld(cellPos).x < transform.position.x && map.CellToWorld(cellPos).y > transform.position.y)
            {
                map.SetTile(cellPos + down, colorBubble);
                ballInTilemapPosition = cellPos + down;
            }
            else if (map.CellToWorld(cellPos).x < transform.position.x && map.CellToWorld(cellPos).y < transform.position.y)
            {
                map.SetTile(cellPos + down, colorBubble);
                ballInTilemapPosition = cellPos + up;
            }
            else if(map.CellToWorld(cellPos).x >= transform.position.x && map.CellToWorld(cellPos).y > transform.position.y)
            {
                map.SetTile(cellPos + downLeft, colorBubble);
                ballInTilemapPosition = cellPos + downLeft;
            }
            else if (map.CellToWorld(cellPos).x >= transform.position.x && map.CellToWorld(cellPos).y < transform.position.y)
            {
                map.SetTile(cellPos + downLeft, colorBubble);
                ballInTilemapPosition = cellPos + upLeft;
            }
        }
        else if (!isPar)
        {
            if (map.CellToWorld(cellPos).x < transform.position.x && map.CellToWorld(cellPos).y > transform.position.y)
            {
                map.SetTile(cellPos + downRight, colorBubble);
                ballInTilemapPosition = cellPos + downRight;
            }
            else if (map.CellToWorld(cellPos).x < transform.position.x && map.CellToWorld(cellPos).y < transform.position.y)
            {
                map.SetTile(cellPos + downRight, colorBubble);
                ballInTilemapPosition = cellPos + upRight;
            }
            else if (map.CellToWorld(cellPos).x >= transform.position.x && map.CellToWorld(cellPos).y > transform.position.y)
            {
                map.SetTile(cellPos + down, colorBubble);
                ballInTilemapPosition = cellPos + down;
            }
            else if (map.CellToWorld(cellPos).x >= transform.position.x && map.CellToWorld(cellPos).y < transform.position.y)
            {
                map.SetTile(cellPos + down, colorBubble);
                ballInTilemapPosition = cellPos + up;
            }
        }

        /// I cycle through the list of neighbors cells (including the node)
        /// I make null those where there is a tile of the same color
        /// as long as the group of cells is greater than 2 units
        List<Vector3Int> tilesConnected = Neighbor(ballInTilemapPosition, new List<Vector3Int>());
        if(tilesConnected.Count > 2)
        {
            /// I assign a score based on the color of the balls hit
            /// I multiply by the number of balls and add them to the score
            if (sr.color == redBubble.color) points = 5;
            else if (sr.color == blueBubble.color) points = 7;
            else if (sr.color == greenBubble.color) points = 10;
            gm.score += points * (tilesConnected.Count - 1);

            foreach (Vector3Int bubble in tilesConnected)
            {
                map.SetTile(bubble, null);
            }
        }

        // Restart the ball
        RestartBall();
    }

    private List<Vector3Int> Neighbor(Vector3Int node, List<Vector3Int> tilesConnected)
    {
        tilesConnected.Add(node);

        // I define the directions of the neighbor cells to the impacted node
        Vector3Int
            left = Vector3Int.left,
            right = Vector3Int.right,
            down = Vector3Int.down,
            downLeft = Vector3Int.down + Vector3Int.left,
            downRight = Vector3Int.down + Vector3Int.right,
            up = Vector3Int.up,
            upLeft = Vector3Int.up + Vector3Int.left,
            upRight = Vector3Int.up + Vector3Int.right;

        // The directions depend on whether it hits a node with odd or even Y coordinate.
        Vector3Int[] directions_when_y_is_even = { left, right, upLeft, up, downLeft, down };
        Vector3Int[] directions_when_y_is_odd = { left, right, up, upRight, down, downRight };
        Vector3Int[] directions = (node.y % 2) == 0 ? directions_when_y_is_even : directions_when_y_is_odd;
        
        foreach (Vector3Int direction in directions)
        {
            Vector3Int neighborPos = node + direction; Debug.Log(map.GetTile<Tile>(neighborPos));

            if (map.GetTile<Tile>(neighborPos) != null && map.GetTile<Tile>(neighborPos).color == sr.color)
            {
                /// Recursively we add balls to the list of connected balls
                if (tilesConnected.Contains(neighborPos)) continue;

                Neighbor(neighborPos, tilesConnected);
            }
        }
        return tilesConnected;
    }

    private void Shoot()
    {
        canShoot = false;

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mouseDir = mousePos - transform.position;
        mouseDir.z = 0.0f;
        mouseDir = mouseDir.normalized;

        if(mousePos.y > transform.position.y)
            rb2d.AddForce(mouseDir * shootForce, ForceMode2D.Impulse);
    }

    private void RestartBall()
    {
        nBalls = cm.nBalls;

        if (nBalls > 0)
        {
            bubbleChamber.SetBallsToPlay(nBalls);
            canShoot = true;
        }
        else
        {
            this.gameObject.SetActive(false);
            gm.Lose();
        }
    }
}
