﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    [HideInInspector] public int score;
    private int highScore;

    private void Awake()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
    }

    // Win Condition
    public void Win()
    {
        Save();
        SceneManager.LoadScene("WinScene");
    }

    // Lose Condition
    public void Lose()
    {
        SceneManager.LoadScene("LoseScene");
    }
    // Save Score
    public void Save()
    {
        Debug.Log("Score: " + score);
        PlayerPrefs.SetInt("Score", score);
        if (score > highScore)
            PlayerPrefs.SetInt("HighScore", score);
    }
}
