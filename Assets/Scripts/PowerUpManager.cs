﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpManager : MonoBehaviour
{
    [SerializeField] private Button horizontalPowerUpButton;
    [SerializeField] private Button verticalPowerUpButton;
    [SerializeField] private Button bombPowerUpButton;

    [SerializeField] private GameObject horizontalZap;
    [SerializeField] private GameObject verticalZap;
    [SerializeField] private GameObject bomb;

    [SerializeField] private GameObject[] activeBalls;

    [HideInInspector] public GameObject ballToPlay;

    public void HorizontalZap()
    {
        DesactiveBallToPlay();

        horizontalPowerUpButton.interactable = false;

        horizontalZap.SetActive(true);
        horizontalZap.transform.position = ballToPlay.transform.position;
        verticalZap.SetActive(false);
        bomb.SetActive(false);
    }

    public void VerticalZap()
    {
        DesactiveBallToPlay();

        verticalPowerUpButton.interactable = false;

        horizontalZap.SetActive(false);
        verticalZap.SetActive(true);
        verticalZap.transform.position = ballToPlay.transform.position;
        bomb.SetActive(false);
    }

    public void Bomb()
    {
        DesactiveBallToPlay();

        bombPowerUpButton.interactable = false;

        horizontalZap.SetActive(false);
        verticalZap.SetActive(false);
        bomb.SetActive(true);
        bomb.transform.position = ballToPlay.transform.position;
    }

    private void DesactiveBallToPlay()
    {
        foreach (GameObject ball in activeBalls)
        {
            if (ball.GetComponent<BubbleController>().enabled == true)
            {
                ballToPlay = ball;
                ball.SetActive(false);
            }
        }
    }
}
