﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelMaker : MonoBehaviour
{
    [SerializeField] private Tilemap map;
    [SerializeField] private Tile[] tiles;
    [SerializeField] private int yMin;

    private void Awake()
    {
        for (int i = 0; i < 9; i++)
        {
            for (int j = yMin; j < 17; j++)
            {
                int randomColor = Random.Range(0, tiles.Length);
                if(i-4 == 4 && j%2 != 0)
                    map.SetTile(new Vector3Int(i - 4, j, 0), null);
                else
                    map.SetTile(new Vector3Int(i - 4, j, 0), tiles[randomColor]);
            }
        }
    }
}
