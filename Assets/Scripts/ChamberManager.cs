﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChamberManager : MonoBehaviour
{
    [HideInInspector] public int maxBalls = 20;
    [HideInInspector] public int nBalls;
}
