**BubbleShooter**

_Programming Test - Bubble Shooter (Unity 2019.4.2f1)_

This test has taken me around 15 hours throughout a week.

Next steps:

- Make Bonus 1
- Add Levels with progresive dificulty
- Add more power ups
- Add more colors
- Add sfx & music
- Add vfx

- Testing all features
- Validate all features

- Switch platform & change settings to build the mobile versión
